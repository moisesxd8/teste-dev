package br.com.eprecise.database;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.eprecise.entidade.Estado;

public interface RepositorioEstado extends JpaRepository<Estado,Long>{
    @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM estado")
    List<Estado> countRegistros(String estado);
}
